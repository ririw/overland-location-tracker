# We use a rust image as the builder, 
# because it contains all the tools needed to compile our application.
FROM rust:latest as builder
# We add our app's cargo toml and lock files, and a dummy src/main.rs
# This allows us to cache our dependencies.
# 
# All code goes in the /app directory.
WORKDIR /app
COPY Cargo.toml Cargo.lock ./
RUN mkdir src
RUN echo "fn main() { println!(\"Hello, world!\"); }" > src/main.rs

# We build for release, which will compile with optimizations.
RUN cargo build --release
# Next, we copy in the src directory and build for real
RUN rm -rf src target/release/overland_location_tracker*
COPY . .
RUN cargo build --release
RUN echo "OK"
# Finally, we switch to a slim image, and copy the built binary from the previous stage.
FROM debian:stable-slim
# Copy our app over, it's called "overland-location-tracker"
WORKDIR /app
# Include checks file for dokku.
COPY CHECKS /app

COPY --from=builder /app/target/release/overland-location-tracker /usr/local/bin/overland-location-tracker

CMD [ "overland-location-tracker" ]