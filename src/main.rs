use rouille::Request;
use rouille::Response;
use rouille::router;
use anyhow::Result;
use std::io;
use std::io::Read;
use serde_json::json;

use postgres::{Client, NoTls};

/**
 * This is a simple web server that you can push JSON to.
 * Push a POST request to /{key} with a JSON body, and it will be saved to the database.
 * The system is set by the following env vars:
 *  - PORT: the port to listen on (default 8080)
 *  - HOST: the host to listen on (default localhost)
 *  - KEY: the key to use for authorization (default "", not for production use)
 *  - DATABASE_URL: the postgresql database to use (default postgresql://postgres:postgres@localhost/postgres)
 * 
 * The database table is called "record" and has two columns:
 *  - id: a serial primary key
 *  - body: a text column
 * 
 * Finally, there are three routes:
 * - GET /: returns "Hello World"
 * - GET /{key}: returns "ok" if key is correct, "not authorized" otherwise
 * - POST /{key}: saves the body of the request to the database if key is correct, "not authorized" otherwise
 */


fn setup_db(db_string: &str) -> () {
    // Create a table called "record" in the database
    let mut conn = Client::connect(db_string, NoTls).unwrap();
    conn.execute(&format!("CREATE TABLE IF NOT EXISTS record (id SERIAL PRIMARY KEY, body TEXT)"), &[]).unwrap();
    conn.close().unwrap();
}


fn address_str() -> String {
    // Create the address string from the PORT environment variable, 
    // defaulting to 8080 if not already set
    let port = std::env::var("PORT").unwrap_or("8080".to_owned());
    let host = std::env::var("HOST").unwrap_or("localhost".to_owned());
    format!("{}:{}", host, port)
}

fn handle(request: &Request, env_key: &str, postgres_url: &str) -> Response {
    router!(request, 
        (GET) (/) => {
            // return simple hello world
            Response::text("Hello World")
        },
        (GET) (/{key: String}) => {
            if key != env_key {
                // The HTTP status for "unauthorized" is 401.
                // If key is not correct, return 401 and "not authorized" text
                return Response::text("not authorized").with_status_code(401);
            }
            // Simply return "ok" if key is correct
            Response::text("ok")
        },
        (POST) (/{key: String}) => {
            if key != env_key {
                // The HTTP status for "unauthorized" is 401.
                // If key is not correct, return 401 and "not authorized" text
                return Response::text("not authorized").with_status_code(401);
            }
            let mut conn = Client::connect(&postgres_url, NoTls).unwrap();
            let body = get_body(request).unwrap();
            // Save the body into the "record" table
            conn.execute("INSERT INTO record (body) VALUES ($1)", &[&body]).unwrap();
            conn.close().unwrap();
            // Respond JSON for {"result": "ok"} message and status code 201
            // See API docs at https://github.com/aaronpk/Overland-iOS#api
            Response::json(&json!({"result": "ok"})).with_status_code(201)
        },
        _ => Response::empty_404()
    )
}

fn get_body(request: &Request) -> Result<String> {
    let mut data = request.data().expect(
        "Oops, body already retrieved, problem in the server");

    let mut buf = Vec::new();
    data.read_to_end(&mut buf)?;
    Ok(String::from_utf8(buf)?)
}

fn main() {
    let env_key = std::env::var("KEY").unwrap_or("".to_owned());
    let postgres_url = std::env::var("DATABASE_URL").unwrap();
    println!("About to set up database");
    setup_db(&postgres_url);
    let server_str = address_str();
    println!("Will start server on {}", server_str);

    rouille::start_server(server_str, move |request| {
        rouille::log(request, io::stdout(), || {
            handle(request, &env_key, &postgres_url)
        })
    });
}
