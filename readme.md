Summary
=======

This is a simple web server that you can push JSON to.
Push a POST request to /{key} with a JSON body, and it will be saved to the database.
The system is set by the following env vars:
 - PORT: the port to listen on (default 8080)
 - HOST: the host to listen on (default localhost)
 - KEY: the key to use for authorization (default "", not for production use)
 - DATABASE_URL: the postgresql database to use (default postgresql://postgres:postgres@localhost/postgres)

The database table is called "record" and has two columns:
 - id: a serial primary key
 - body: a text column

Finally, there are three routes:
 - GET /: returns "Hello World"
 - GET /{key}: returns "ok" if key is correct, "not authorized" otherwise
 - POST /{key}: saves the body of the request to the database if key is correct, "not authorized" otherwise

Deployment notes for richard :)
===============================

1. Deployed into Dokku
2. Linked to a postgres app
3. Config set through dokku

 - Override HOST to bind externally: `dokku config:set overland-location-tracker HOST=0.0.0.0`
 - Set KEY: `dokku config:set overland-location-tracker KEY=XXXX`



Repo URL: dokku@richardweiss.net:overland-location-tracker

Useful for checking connectivity 
--------------------------------
```
apt update; apt install -y curl
curl localhost:8080
curl localhost:5000
```